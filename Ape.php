<?php 

//saat menggunakan require untuk menghubungkan semua file. hanya butuh 1 kali


class Ape extends Animal{
	public function __construct($name, $legs=2,$cold_blooded='no'){
		$this->name = $name;
		$this->legs = $legs;
		$this->cold_blooded = $cold_blooded;
	}
	public function yell(){
		return "auooo";
	}

}
